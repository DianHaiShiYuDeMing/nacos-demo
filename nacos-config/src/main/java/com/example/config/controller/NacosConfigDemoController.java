package com.example.config.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
//@RefreshScope//使用注解NacosValue  可以不使用这个注解
public class NacosConfigDemoController {

    //${test.name:test} 中: 后边的test 为默认 值
    //@NacosValue(value = "${test.name:aa}",autoRefreshed = true)
    @Value(value = "${test.name:aa}")
    private String testName;

    //${test.name:test} 中: 后边的test 为默认 值
    //@NacosValue(value = "${test.age:11}",autoRefreshed = true)
    @Value(value = "${test.age}")
    private int testAge;

    //${test.name:test} 中: 后边的test 为默认 值
    //@NacosValue(value = "${test.boolean:false}",autoRefreshed = true)
    @Value(value = "${test.boolean:false}")
    private boolean testBoolean;

    @GetMapping("/testName")
    public String testName() {
        return testName;
    }

    @GetMapping("/testAge")
    public int testAge() {
        return testAge;
    }

    @GetMapping("/testBoolean")
    public boolean testBoolean() {
        return testBoolean;
    }

}
