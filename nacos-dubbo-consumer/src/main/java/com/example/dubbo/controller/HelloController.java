package com.example.dubbo.controller;

import com.example.dubbo.api.HelloService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @DubboReference
    private HelloService helloService;

    @GetMapping("/syHello")
    public String syHello(String name){

        return helloService.syHello(name);
    }

}
