package com.example.dubbo;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient// 开启nacos 服务发现
@DubboComponentScan
public class NacosDubboConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosDubboConsumerApplication.class, args);
    }
}
