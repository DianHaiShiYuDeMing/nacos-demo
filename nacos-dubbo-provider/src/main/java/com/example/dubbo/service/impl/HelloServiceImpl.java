package com.example.dubbo.service.impl;

import com.example.dubbo.api.HelloService;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService
public class HelloServiceImpl implements HelloService {

    public String syHello(String name) {
        return "hello "+name;
    }
}