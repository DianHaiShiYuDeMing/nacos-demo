package com.example.fegin.controller;

import com.example.fegin.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeginContoller {

    @Autowired
    private DemoService demoService;

    @GetMapping("/test/{id}")
    public Object test(@PathVariable String id) {

        Object forObject = demoService.test(id);

        return forObject;
    }
}
