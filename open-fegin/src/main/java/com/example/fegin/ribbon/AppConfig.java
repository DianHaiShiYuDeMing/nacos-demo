package com.example.fegin.ribbon;

import com.netflix.loadbalancer.IRule;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * springcloud ribbon 负载均衡 配置
 *
 */
@SpringBootConfiguration
public class AppConfig {

    @Bean //负载 自定义机制
    public IRule getRule() {
        return new CunsumerRule();
    }

   /* @Bean //随机机制   默认轮询
    public IRule getRule() {
        return new RandomRule();
    }*/
}