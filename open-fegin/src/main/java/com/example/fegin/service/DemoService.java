package com.example.fegin.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "provider",fallback = RemoteHystrix.class)
public interface DemoService {

    @GetMapping("/demo/test/{id}")
    String test(@PathVariable(value = "id") String id);
}
