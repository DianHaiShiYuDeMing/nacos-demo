package com.example.fegin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient// 开启nacos 服务发现
@EnableFeignClients(basePackages = "com.example.fegin")
public class OpenFeginApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenFeginApplication.class, args);
    }

}
