package com.example.gateway.config;

import com.example.gateway.filter.DemoGatewayFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

    /**
     * 路由配置
     *
     * @param builder
     * @return
     */
    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder.routes().route(r ->
                r.path("/**")
                        .filters(f -> f.filter(new DemoGatewayFilter()))
                        .uri("lb://open-fegin")
                        .id("ssss")
        ).build();
    }
}
