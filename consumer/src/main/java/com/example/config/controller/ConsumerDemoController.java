package com.example.config.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
@RequestMapping("/consumer")
public class ConsumerDemoController {

    @Resource(name="restTemplate")
    private RestTemplate restTemplate;

    @GetMapping("/test/{id}")
    public Object test(@PathVariable String id) {
        String forObject = restTemplate.getForObject("http://provider/demo/test/111", String.class);
        return forObject;
    }


}
