package com.example.config.config.ribbon;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.Server;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 自定义 负载均衡策略
 */

public class CunsumerRule extends AbstractLoadBalancerRule {

    private volatile int i = 1;

    public void initWithNiwsConfig(IClientConfig iClientConfig) {
    }

    public Server choose(Object o) {
        //获取所有可用服务
        List<Server> reachableServers = getLoadBalancer().getReachableServers();

        if (CollectionUtils.isEmpty(reachableServers)) {
            return null;
        }
        for (Server reachableServer : reachableServers) {
            int index = this.i % reachableServers.size();
            i++;
            if (i > 100000000) {
                i = 1;
            }
            System.out.println("CunsumerRule index:" + index + " i:" + i);
            return reachableServers.get(index);
        }
        return null;
    }
}
