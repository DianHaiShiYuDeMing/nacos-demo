package com.example.config.config.ribbon;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * springcloud ribbon 负载均衡 配置
 *
 */
@SpringBootConfiguration
public class AppConfig {

    @Bean
    @LoadBalanced//负载均衡
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

  /*  @Bean //负载 自定义机制
    public IRule getRule() {
        return new CunsumerRule();
    }*/

    @Bean //随机机制   默认轮询
    public IRule getRule() {
        return new RandomRule();
    }
}


