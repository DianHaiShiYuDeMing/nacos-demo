package com.example.provider.service;

import org.springframework.stereotype.Service;

@Service("demoService")
public class DemoServiceImpl {
    public String test(String id) {
        return "111" + id;
    }
}
