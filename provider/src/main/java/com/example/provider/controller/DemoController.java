package com.example.provider.controller;

import com.example.provider.service.DemoServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @Resource(name = "demoService")
    private DemoServiceImpl demoService;

    @GetMapping("/test/{id}")
    public String test(@PathVariable String id) {

        //测试超时时间 demo
        try {
            System.out.println("等待中");
            Thread.sleep(9000);
            System.out.println("等待结束");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return demoService.test(id);
    }
}
